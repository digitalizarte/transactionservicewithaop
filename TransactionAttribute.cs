﻿namespace TransactionServiceWithAOP
{
    using System;

    /// <summary>
    /// Atributo para indicar que se requiere transacción en el método.
    /// </summary>
    public class TransactionAttribute : Attribute
    {
    }
}
