﻿namespace TransactionServiceWithAOP
{
    using Autofac;

    /// <summary>
    /// Módulo que regista el interceptor.
    /// </summary>
    public class AopTransactionmodule : Autofac.Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AopTransactionInterceptor>().AsSelf().AsImplementedInterfaces();
        }
    }
}