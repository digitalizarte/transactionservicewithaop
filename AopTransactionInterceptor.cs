﻿namespace TransactionServiceWithAOP
{
    using System;
    using System.Linq;
    using System.Transactions;
    using Castle.DynamicProxy;

    /// <summary>
    /// Interceptor para ejecutar metodos en transacion evalua si el método requiere 
    /// y no se esta ejecutando en transacción, inicia una transacción y la confirma.
    /// </summary>
    public class AopTransactionInterceptor : IInterceptor
    {
        #region Declarations

        /// <summary>
        /// Tipo para indicar que un método se tiene que ejecutar en transacción.
        /// </summary>
        private readonly Type type = typeof(TransactionAttribute);

        #endregion

        #region Methods

        /// <summary>
        /// Ejecuta el método.
        /// Verifica si el método esta marcado para ejecutarse en transación
        /// y crea una si no hay una transacción ya iniciada.
        /// </summary>
        /// <param name="invocation">Encapsulates an invocation of a proxied method.</param>
        public void Intercept(IInvocation invocation)
        {
            if (invocation == null)
            {
                throw new ArgumentNullException(nameof(invocation));
            }

            bool requireTransaction = invocation.MethodInvocationTarget
                .GetCustomAttributes(this.type, true).Any();

            // Verifica si se requiere transación.
            if (requireTransaction)
            {
                if (Transaction.Current == null)
                {
                    // Inicia la transacción.
                    using (TransactionScope txn = new TransactionScope())
                    {
                        // Ejecuta el método.
                        invocation.Proceed();

                        // Confirma la transacción.
                        txn.Complete();
                    }
                }
                else
                {
                    // Ejecuta el método.
                    invocation.Proceed();
                }
            }
            else
            {
                // Ejecuta el método.
                invocation.Proceed();
            }
        }

        #endregion
    }
}