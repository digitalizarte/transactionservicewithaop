﻿namespace TransactionServiceWithAOP
{
    using System;
    using Autofac;
    using Autofac.Builder;
    using Autofac.Extras.DynamicProxy;

    /// <summary>
    /// Extensiones para facilitar el registro en autofac.
    /// </summary>
    public static class AutofacExtensions
    {
        #region Declarations

        /// <summary>
        /// Referencia del tipo de la clase interceptor.
        /// </summary>
        private static readonly Type InterceptorServiceTypes = typeof(AopTransactionInterceptor);

        #endregion

        #region Methods

        /// <summary>
        /// Registra el módulo de las transacciones.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        public static void RegisterTransactionModule(this ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.RegisterModule<AopTransactionmodule>();
        }

        /// <summary>
        /// Habilita las transacciones automaticas con el interceptor.
        /// </summary>
        /// <typeparam name="TLimit">Registration limit type.</typeparam>
        /// <typeparam name="TActivatorData">Activator data type.</typeparam>
        /// <typeparam name="TRegistrionStyle">Registration style.</typeparam>
        /// <param name="builder">The builder through which components can be registered.</param>
        /// <returns>El regitro.</returns>
        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrionStyle>
            EnableTransactionInterception<TLimit, TActivatorData, TRegistrionStyle>(this IRegistrationBuilder<TLimit, TActivatorData, TRegistrionStyle> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.EnableInterfaceInterceptors().InterceptedBy(InterceptorServiceTypes);
        }

        /// <summary>
        /// Habilita las transacciones automaticas con el interceptor.
        /// </summary>
        /// <typeparam name="TLimit">Registration limit type.</typeparam>
        /// <typeparam name="TActivatorData">Activator data type.</typeparam>
        /// <typeparam name="TRegistrionStyle">Registration style.</typeparam>
        /// <param name="builder">The builder through which components can be registered.</param>
        /// <returns>El regitro.</returns>
        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrionStyle>
            EnableTransaction<TLimit, TActivatorData, TRegistrionStyle>(this IRegistrationBuilder<TLimit, TActivatorData, TRegistrionStyle> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.EnableInterfaceInterceptors().InterceptedBy(InterceptorServiceTypes);
        }

        #endregion
    }
}